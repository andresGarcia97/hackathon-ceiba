package com.co.restcontroller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.co.dominio.Proveedor;
import com.co.excepcion.Excepcion;
import com.co.validaciones.ValidacionProveedorDominio;

import lombok.NoArgsConstructor;

@RestController
@NoArgsConstructor
@RequestMapping("/proveedores")
public class ProveedorRestController {

	@Autowired
	private ValidacionProveedorDominio proveedorValidado;

	private Map<String,Object> response = new HashMap<>();
	private HttpStatus status;
	private String advertencia = "Advertencia";

	@GetMapping("/listar")
	public List<Proveedor> listAll(){
		return proveedorValidado.listaValidadaProveedores();
	}

	@GetMapping("/proveedor/{id}")
	public Proveedor finById (@PathVariable String id) throws Excepcion {
		Proveedor retorno;
		if(proveedorValidado.consultaIdValidadaProveedor(id) == null) {
			retorno = null;
		}
		else {
			retorno = proveedorValidado.consultaIdValidadaProveedor(id);
		}
		return retorno;
	}
	
	@PostMapping("/insertar")
	public ResponseEntity<?> insert(@RequestBody Proveedor proveedor) throws BindException, Excepcion {
		if(proveedorValidado.consultaValidadaProveedor(proveedor) != null) {
			response.put(advertencia, "Ya existe un proveedor con esta Identificacion: "+ proveedor.getId());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			proveedorValidado.creacionValidadaProveedor(proveedor);
			response.put(advertencia, "La informacion se ha insertado exitosamente");
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}
	
	@PutMapping("/actualizar")
	public ResponseEntity<?> actualizar(@RequestBody Proveedor proveedor) throws BindException, Excepcion {
		if(proveedorValidado.consultaValidadaProveedor(proveedor) == null) {
			response.put(advertencia, "No existe un cliente con esta Identificacion, "+proveedor.getId());
			status = HttpStatus.BAD_REQUEST;
		}
		else {
			proveedorValidado.actualizacionValidadaProveedor(proveedor);
			response.put(advertencia, "La informacion se ha actualizado exitosamente");
			status = HttpStatus.CREATED;
		}
		return new ResponseEntity<>(response, status);
	}
}
