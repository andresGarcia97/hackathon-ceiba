package com.co.dominio;

import java.util.Calendar;
import com.co.utilitarios.UtilObjeto;
import com.co.excepcion.Excepcion;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Proveedor {
	
	private String id;
	private String nombre;
	private Calendar fechaRegistro;
	private int telefono;
	private String direccion;
	
	public Proveedor(String id, String nombre, Calendar fechaRegistro, int telefono, String direccion) {
		super();
		this.id = UtilObjeto.utilObjeto().evitarNulos(id, "").trim();
		this.nombre = UtilObjeto.utilObjeto().evitarNulos(nombre, "").trim();
		this.fechaRegistro = UtilObjeto.utilObjeto().evitarNulos(fechaRegistro, Calendar.getInstance());
		this.telefono = telefono;
		this.direccion = UtilObjeto.utilObjeto().evitarNulos(direccion, "").trim();
	}
	
	public boolean validarNombre (String nombre) throws Excepcion {
		boolean nombreValidado = false;
		if(nombre.length() < 5) {
			throw new Excepcion("minimo 5 caracteres");
		}
		else {
			String nombrePalindromo = nombre.replace(" ", "");
			int tamano = nombrePalindromo.length()-1;
			int contador = 0;
			char [] cadena = nombrePalindromo.toCharArray();
			
			for (int i = 0; i < nombrePalindromo.length(); i++) {
				if(cadena[i] == cadena[tamano-i]) {
					contador++;
				}
			}
			
			if(contador == tamano+1) {
				throw new Excepcion("es una palabra palindroma");
			}
			else {
				nombreValidado = true;
			}
		}
		return nombreValidado;
	}
	
	public boolean validarTelefono (int telefono) throws Excepcion {
		boolean telefonoValidado = false;
		String telefonoString = Integer.toString(telefono);
		if(telefonoString.length() != 7) {
			throw new Excepcion("El telefono debe ser un numero de 7 caracteres");
		}
		else {
			telefonoValidado = true;
		}
		return telefonoValidado;
	}
	
	public void validarDireccion (String direccion) throws Excepcion {
		if(direccion.startsWith("CL")) {
			int contador = 0;
			char [] cadena = direccion.toLowerCase().trim().toCharArray();
			for (int i = 0; i < cadena.length; i++) {
				if(cadena [i] == 'a' || cadena [i] == 'e' || cadena [i] == 'i' || cadena [i] == 'o' ||
				   cadena [i] == 'u') {
					contador++;
				}
			}
			if(contador <= 3) {
				throw new Excepcion("no tiene por lo menos 3 vocales");
			}
			}
		else {
			throw new Excepcion("no empieza con CL ");
			
		}
		}
	
	public void validarFecha(Calendar fecha) throws Excepcion {
		if(fecha.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			throw new Excepcion("no se pueden ingresar provedores el dia domingo");
		}
	}
	
	public void validarProveedor(Proveedor proveedor) throws Excepcion  {
		validarNombre(proveedor.getNombre());
		validarTelefono(proveedor.getTelefono());
		validarDireccion(proveedor.getDireccion());
		validarFecha(proveedor.getFechaRegistro());
	}
	}
