package com.co.servicio;

import java.util.List;
import com.co.dominio.Proveedor;

public interface IProveedorServicio {

	List<Proveedor> listAll();
	
	Proveedor findById (String id);
	
	void save(Proveedor proveedor);

	void update(Proveedor proveedor);
}
