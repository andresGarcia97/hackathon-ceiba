package com.co.servicio.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.co.converter.impl.ProveedorConverterImpl;
import com.co.dominio.Proveedor;
import com.co.repositorio.IProveedorRepositorio;
import com.co.servicio.IProveedorServicio;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
public class ProveedorServicioImpl implements IProveedorServicio {

	@Autowired
	private IProveedorRepositorio proveedorRepo;
	
	@Autowired
	private ProveedorConverterImpl converter;
	
	@Override
	public List<Proveedor> listAll() {
		return converter.entityToModel(proveedorRepo.findAll());
	}

	@Override
	public Proveedor findById(String id) {
		return converter.entityToModel(proveedorRepo.findById(id).orElse(null));
	}

	@Override
	public void save(Proveedor proveedor) {
		proveedorRepo.save(converter.modelToEntity(proveedor));
	}

	@Override
	public void update(Proveedor proveedor) {
		proveedorRepo.save(converter.modelToEntity(proveedor));
	}
	 
	
}
