package com.co.repositorio;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.co.entity.ProveedorEntity;

@Repository
public interface IProveedorRepositorio extends JpaRepository<ProveedorEntity, Serializable> {

}
