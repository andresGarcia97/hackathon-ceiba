package com.co.entity;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "proveedor")
@NoArgsConstructor
public class ProveedorEntity {
	
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name="nombre", nullable=false)
	private String nombre;
	
	@Column(name="fecha_registro", nullable=false)
	@Temporal(TemporalType.DATE)
	private Calendar fechaRegistro;
	
	@Column(name = "telefono", nullable=false)
	private int telefono;
	
	@Column(name = "direccion", nullable=false)
	private String direccion;

}
