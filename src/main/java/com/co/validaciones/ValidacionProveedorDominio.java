package com.co.validaciones;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.co.dominio.Proveedor;
import com.co.excepcion.Excepcion;
import com.co.servicio.IProveedorServicio;

@Component
public class ValidacionProveedorDominio {
	
	@Autowired
	private IProveedorServicio proveedorServicio;
	
	private void validacionProveedor(Proveedor proveedor) throws Excepcion {
		Proveedor validator = new Proveedor(proveedor.getId(), proveedor.getNombre(), proveedor.getFechaRegistro(), proveedor.getTelefono(), proveedor.getDireccion());
		validator.validarProveedor(validator);
	}
	
	public Proveedor consultaIdValidadaProveedor(String id) throws Excepcion {
		Proveedor retorno = proveedorServicio.findById(id) ;
		if(retorno != null) {
			validacionProveedor(retorno);
		}
		return retorno;
	}
	
	public Proveedor consultaValidadaProveedor(Proveedor proveedor) throws Excepcion {
		validacionProveedor(proveedor);
		return proveedorServicio.findById(proveedor.getId());
	}
	
	public void actualizacionValidadaProveedor (Proveedor proveedor) throws Excepcion {
		validacionProveedor(proveedor);
		proveedorServicio.update(proveedor);
	}
	
	public void creacionValidadaProveedor (Proveedor proveedor) throws Excepcion {
		validacionProveedor(proveedor);
		proveedorServicio.save(proveedor);
	}
	
	//no se puso a validar cada elemento, falta por hacerlo con programacion funcional
	public List<Proveedor> listaValidadaProveedores(){
		return proveedorServicio.listAll();
	}

}
