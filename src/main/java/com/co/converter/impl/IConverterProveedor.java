package com.co.converter.impl;

import java.util.List;

import com.co.dominio.Proveedor;
import com.co.entity.ProveedorEntity;
import com.co.excepcion.Excepcion;

public interface IConverterProveedor {

	Proveedor entityToModel(ProveedorEntity entidadProveedor);
	
	ProveedorEntity modelToEntity (Proveedor proveedor);
	
	List<Proveedor> entityToModel (List<ProveedorEntity> listaProveedores);
}
