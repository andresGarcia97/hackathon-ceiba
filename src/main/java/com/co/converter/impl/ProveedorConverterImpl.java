package com.co.converter.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import com.co.dominio.Proveedor;
import com.co.entity.ProveedorEntity;

@Component
public class ProveedorConverterImpl implements IConverterProveedor{

	@Override
	public Proveedor entityToModel(ProveedorEntity entidad){
		Proveedor retorno;
		if(entidad == null) {
			retorno = null;
		}
		else {
			retorno = new Proveedor(entidad.getId(), entidad.getNombre(), entidad.getFechaRegistro(), entidad.getTelefono(), entidad.getDireccion());
		}
		return retorno;
	}

	@Override
	public ProveedorEntity modelToEntity(Proveedor proveedor) {
		ProveedorEntity entidad = new ProveedorEntity();
		entidad.setId(proveedor.getId());
		entidad.setNombre(proveedor.getNombre());
		entidad.setFechaRegistro(proveedor.getFechaRegistro());
		entidad.setTelefono(proveedor.getTelefono());
		entidad.setDireccion(proveedor.getDireccion());
		return entidad;
	}

	@Override
	public List<Proveedor> entityToModel(List<ProveedorEntity> listaProveedores) {
		List<Proveedor> proveedores = new ArrayList<Proveedor>(listaProveedores.size());
		listaProveedores.forEach((entity) -> {
			proveedores.add(entityToModel(entity));
		});
		return proveedores;
	}

}
