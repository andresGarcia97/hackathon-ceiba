package com.co;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SutrabajoaseguradoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SutrabajoaseguradoApplication.class, args);
	}

}
