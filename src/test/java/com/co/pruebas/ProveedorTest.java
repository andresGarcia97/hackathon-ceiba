package com.co.pruebas;

import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.co.dominio.Proveedor;
import com.co.excepcion.Excepcion;

public class ProveedorTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	 @Test
	 public void validarNombreCorrectoTest() throws Excepcion {
	 //arrange
	 Proveedor proveedor= new Proveedor("12ffs6","Exito",null,1234567,"CLabcdea");
	 //act
	 boolean resultado = proveedor.validarNombre(proveedor.getNombre());
	 //assert
	 assertTrue(resultado);
	 }
	 
	 @Test
	 public void validarNombreIncorrectoTest() throws Excepcion {
	 //arrange
	 Proveedor proveedor= new Proveedor("12ffs6","Exi",null,1234567,"CLabcdea");
	 Proveedor proveedor2= new Proveedor("12ffs6","anita lava la tina",null,1234567,"CLabcdea");
	 //act
	 exception.expect(Excepcion.class);
	 boolean resultadoMenor5 = proveedor.validarNombre(proveedor.getNombre());
	 boolean resultadoPalindroma = proveedor2.validarNombre(proveedor2.getNombre());
	 //assert
	 assertFalse(resultadoMenor5);
	 assertFalse(resultadoPalindroma);
	 }
	 
	 @Test
	 public void validarTelefonoCorrectoTest() throws Excepcion {
	 //arrange
	 Proveedor proveedor= new Proveedor("12ffs6","Exito",null,1234567,"CLabcdea");
	 //act
	 boolean resultado = proveedor.validarTelefono(proveedor.getTelefono());
	 //assert
	 assertTrue(resultado);
	 }
	 
	 @Test
	 public void validarTelefonoIncorrectoTest() throws Excepcion {
	 //arrange
	 Proveedor proveedor= new Proveedor("12ffs6","Exito",null,12345678,"CLabcdea");
	 Proveedor proveedor2= new Proveedor("12ffs6","anita lava la tina",null,12345,"CLabcdea");
	 //act
	 exception.expect(Excepcion.class);
	 boolean resultado = proveedor.validarTelefono(proveedor.getTelefono());
	 boolean resultado2 = proveedor2.validarTelefono(proveedor2.getTelefono());
	 //assert
	 assertFalse(resultado);
	 assertFalse(resultado2);
	 }


}
